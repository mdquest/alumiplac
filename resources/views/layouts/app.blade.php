<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" ng-app="appAlumiplac">
<head>
  <meta charset="utf-8">
  
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">


  @if(isset($db_page))
    <meta content="{{ $db_page->keywords }}" name="keywords">
    <meta content="{{ $db_page->description }}" name="description">
    <title>{{ $company->name }} - {{ $db_page->name }}</title>
    <link rev="made" href="mailto:{{ $company->default_email }}">
    <meta property="og:title" content="{{ $company->description }}">

  @else
    <title>Alumiplac - Home</title>
  @endif

  <meta property="{{ url('/') }}">
  <base href="{{ url('/') }}">


  <!-- Favicons -->
  <link href="{{ asset('public/favicon.ico') }}" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset('public/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset('public/lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('public/lib/animate/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('public/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('public/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('public/lib/lightbox/css/lightbox.min.css') }}" rel="stylesheet">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

  <!-- Main Stylesheet File -->
  <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Bizdb_page
    Theme URL: https://bootstrapmade.com/bizdb_page-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>
<input type="hidden" name="base_url" id="base_url" value="{{ url('/') }}">


<?php 
  if(!isset($isIndex))
  {
    $style = "style='background: rgba(0, 0, 0, 0.9); padding: 20px 0; height: 72px;  transition: all 0.5s;'";
  }else
  {
    $style = "style=''";
  }
?>

  <!--==========================
    Header
  ============================-->
  <header id="header" <?php print($style); ?>>
    <div class="container-fluid">

      <div id="logo" class="pull-left">
        <!--<h1><a href="#intro" class="scrollto">co</a></h1>-->
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="#intro"><img src="{{ url($company->image) }}" style="max-height: 40px;" alt="Logo da {{ $company->name }}" title="" /></a>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li><a href="#intro">Home</a></li>
          <li><a href="#about">Sobre</a></li>
          <li class="menu-has-children"><a href="#portfolio">Produtos</a>
            <ul>
              @foreach($products as $product)
                <li><a href="{{ url('produtos/'.$product->url) }}">{{ $product->name }}</a></li>
              @endforeach
            </ul>
          </li>
          <li><a href="#contact">Fale conosco</a></li>
          <li><a href="{{ route('app_estimate_index') }}">Orçamento</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->


        @yield('content') 

         <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-info">
            <h3>{{ $company->name }}</h3>
            <p>Alumiplac atua no Mercado desde 1987, buscamos superar as expectativas em Produtos, sempre buscando inovações.</p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Menu</h4>
            <ul>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Home</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Sobre</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Produtos</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Fale conosco</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="{{ route('app_estimate_index') }}">Orçamento</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contato</h4>
            <p>
              Rua Bárbara Heliodora, 82 <br>
              Utinga,Santo André, SP<br>
              Brasil <br>
              <strong>Tel:</strong> 11 4461-1766<br>
              <strong>Email:</strong>{{ $company->default_email }}<br>
            </p>

            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="https://www.linkedin.com/company/alumiplac-industria-e-comercio/?originalSubdomain=pt" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>

          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter" ng-controller="NewsLetterController">
            <h4>Newsletter</h4>
            <p>Receba e-mails contendo materiais e novidades.</p>
            <form method="post" ng-submit="processFormInsert()" name="formNewLetter" id="formNewLetter">
              <input type="email" name="email" ng-model="formData.email" required="required"><input type="submit"  value="enviar">
            </form>
            <p ng-show="inserted">Obrigado por se inscrever</p>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Alumiplac</strong>. Todos os direitos reservados
      </div>
      <div class="credits">
        @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Painel administrativo</a>
                    @else
                        <a href="{{ url('/login') }}">Área logada</a>
                    @endif
                </div>
            @endif
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Bizdb_page
        
        Best <a href="https://bootstrapmade.com/">Bootstrap Templates</a> by BootstrapMade
        -->
      </div>
    </div>
  </footer><!-- #footer -->


  
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="{{ asset('public/lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('public/lib/jquery/jquery-migrate.min.js') }}"></script>
  <script src="{{ asset('public/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('public/lib/easing/easing.min.js') }}"></script>
  <script src="{{ asset('public/lib/superfish/hoverIntent.js') }}"></script>
  <script src="{{ asset('public/lib/superfish/superfish.min.js') }}"></script>
  <script src="{{ asset('public/lib/wow/wow.min.js') }}"></script>
  <script src="{{ asset('public/lib/waypoints/waypoints.min.js') }}"></script>
  <script src="{{ asset('public/lib/counterup/counterup.min.js') }}"></script>
  <script src="{{ asset('public/lib/owlcarousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('public/lib/isotope/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('public/lib/lightbox/js/lightbox.min.js') }}"></script>
  <script src="{{ asset('public/lib/touchSwipe/jquery.touchSwipe.min.js') }}"></script>
  <!-- Contact Form JavaScript File -->
  <!--<script src="{{ asset('public/contactform/contactform.js') }}"></script>-->

  <!-- Template Main Javascript File -->
  <script src="{{ asset('public/js/main.js') }}"></script>

<!-- Angular -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>


  <script type="text/javascript" src="{{ asset('public/js/scopes/simple.js') }}"></script>

  <script type="text/javascript">
    $('.selectpicker').selectpicker({
      style: 'btn-secondary',
      size: 4
    });

  </script>

</body>
</html>
