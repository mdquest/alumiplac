
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" ng-app="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }} | Acesso à plataforma</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('public/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('public/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('public/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('public/dist/css/AdminLTE.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('public/plugins/iCheck/square/blue.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style type="text/css">
      nav
      {
         background-color: #101113!important;
      }

      body
      {
        background: #f4f4f4!important;
      }

      .text-white
      {
        color: #fff!important;
      }

      .btn-alumiplac
      {
        font-family: "Montserrat", sans-serif;
        font-weight: 500;
        font-size: 16px;
        letter-spacing: 1px;
        display: inline-block;
        padding: 8px 32px;
        transition: 0.5s;
        margin: 10px;
        color: #fff;
        background: #872A2A;
      }

       .btn-alumiplac-link
       {
          color: #872A2A;
       }

      .btn-alumiplac:hover,
      .btn-alumiplac:focus
      {
        background-color: #FFFFFF;
        color: #872A2A;
        border-color: #872A2A;
      }
    </style>
</head>

<body class="sidebar-mini wysihtml5-supported fixed sidebar-mini-expand-feature sidebar-collapse skin-black">
    <div id="app">
        <nav class="navbar navbar-default  navbar-static-top" >
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand text-white" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}" class="text-white">Login</a></li>

                            @if(\App\Helpers\RegisterHelper::instance()->is_allow_user_register())
                            <li><a href="{{ route('register') }}" class="text-white">Registrar</a></li>
                            @endif
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Sair
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <!-- Angular -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>

<script type="text/javascript" src="{{ asset('public/js/chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/angular-chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/ng-table.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/dirPagination.js') }}"></script>

<script type="text/javascript" src="{{ asset('public/bower_components/cpf_cnpj/build/cpf.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/bower_components/ng-cpf-cnpj/lib/ngCpfCnpj.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/validation.js') }}"></script>

</body>
</html>
