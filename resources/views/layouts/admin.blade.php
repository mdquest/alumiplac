<!DOCTYPE html>
<html ng-app="appAlumiplac">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }} | Minha Área</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('public/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('public/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('public/bower_components/Ionicons/css/ionicons.min.css') }}">

  <link rel="icon" href="{{ asset('public/new_engage.ico') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('public/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('public/dist/css/skins/_all-skins.min.css') }}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{ asset('public/bower_components/morris.js/morris.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('public/bower_components/jvectormap/jquery-jvectormap.css') }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ asset('public/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('public/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

  <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/iCheck/flat/blue.css') }}">

    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-select.min.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style type="text/css">
    .hideAfter5Seconds {
    -moz-animation: cssAnimation 0s ease-out 5s forwards;
    /* Firefox */
    -webkit-animation: cssAnimation 0s ease-out 5s forwards;
    /* Safari and Chrome */
    -o-animation: cssAnimation 0s ease-out 5s forwards;
    /* Opera */
    animation: cssAnimation 0s ease-out 5s forwards;
    -webkit-animation-fill-mode: forwards;
    animation-fill-mode: forwards;
  }
  </style>
</head>
<body class="sidebar-mini wysihtml5-supported fixed sidebar-mini-expand-feature skin-black-light" style="height: auto; min-height: 100%;">

<input type="hidden" name="base_url" id="base_url" value="{{ asset('/') }}">



<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>G</b>E</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>{{ config('app.name', 'Laravel') }}</b>&nbsp;Admin</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="tasks-menu">
            <a href="{{ route('home_mail_contact') }}" class="dropdown-toggle" >
              <i class="fa fa-envelope-o"></i>
            </a>
            
          </li>

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ url($user->image) }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ $user->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{ url($user->image) }}" class="img-circle" alt="User Image">

                <p>
                  {{ $user->name }}
                  <small>Membro desde {{ date('Y', strtotime($user->created_at)) }}</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="{{ route('home') }}">Home</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Ajuda</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="{{ route('about_this_project') }}">Sobre</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ url('home/profile/'.$user->id) }}" class="btn btn-default btn-flat">Meu perfil</a>
                </div>
                <div class="pull-right">
                  <a class="btn btn-default btn-flat"href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                  Sair</a>
                </div>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="{{ route('home_settings_company') }}"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ url($user->image) }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ $user->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu Principal</li>
        <li class="active">
          
          <a href="{{ route('home') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
     
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-cog"></i> <span>Configurações</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="{{ route('home_settings_company') }}"><i class="fa fa-circle-o"></i>Empresa</a></li>
            <li><a href="{{ route('home_settings_analytics') }}"><i class="fa fa-circle-o"></i>Analytics</a></li>
            <!--<li><a href="{{ route('home_cubic_index') }}"><i class="fa fa-circle-o"></i>Cubic Shot</a></li>-->
            <li><a href="#" data-toggle="modal" data-target="#modalDesktop"><i class="fa fa-circle-o"></i>Área de trabalho</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Usuários</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="#"><i class="fa fa-circle-o"></i>Todos</a></li>
            
            <!--<li><a href="#"><i class="fa fa-circle-o"></i>Clientes</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Prestador de serviço</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Fornecedor</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Transportadora</a></li>-->
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-file"></i> <span>Páginas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="{{ route('home_pages_index') }}"><i class="fa fa-circle-o"></i>Páginas</a></li>
            \<!--<li><a href="#"><i class="fa fa-circle-o"></i>Menus</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Produtos</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Blog</a></li>-->
          </ul>
        </li>

         <li>
          <a href="{{ route('home_mail_contact') }}">
            <i class="fa fa-envelope"></i> <span>Mensagens</span>
           
          </a>
        </li>

        <!--<li>
          <a href="#">
           <i class="fa fa-cloud"></i><span>Nuvem</span>
          </a>
        </li>-->

        <li>
          <a href="{{ route('about_this_project') }}">
           <i class="fa fa-life-buoy"></i><span>Sobre</span>
          </a>
        </li>
       
         <!--<li>
          <a href="#">
            <i class="fa fa-calendar"></i> <span>Agenda</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>-->
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
 @yield('content')
  </div>
  <!-- /.content-wrapper -->
  <!-- <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer> -->

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>SGPA/Cubic</b> 2.4.0
    </div>
    <strong>Copyright © 2018 <a href="#" target="_blank">GE</a>.</strong> Todos os direitos reservados.
  </footer>


<div class="modal fade" id="modalDesktop" name="modalDesktop">
  <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Opções da área de trabalho</h4>
              </div>
              <div class="modal-body">
                <p>One fine body…</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary">Salvar</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modalMessage">
          <div class="modal-dialog  modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="modalMessageTitle">Status da operação</h4>
              </div>
              <div class="modal-body">
                <p id="modalMessageBody">Operação concluída com sucesso</p>
              </div>
              <div class="modal-footer">
              <button type="button" id="modalMessageButtonOk" class="btn" data-dismiss="modal">fechar</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modalErrorMessage">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Status da operação</h4>
              </div>
              <div class="modal-body">
                <p>Ocorreu um erro ao realizar a operação</p>
              </div>
              <div class="modal-footer">
              <button type="button" class="btn btn-warning" data-dismiss="modal">fechar</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('public/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('public/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('public/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="{{ asset('public/bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('public/bower_components/morris.js/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('public/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('public/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('public/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('public/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('public/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('public/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('public/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('public/bower_components/fastclick/lib/fastclick.js') }}"></script>

<script src="{{ asset('public/bower_components/ckeditor/ckeditor.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('public/dist/js/demo.js') }}"></script>

<!-- Angular -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
<!--<script type="text/javascript" href="{{ asset('public/js/angular.min.js') }}"></script>-->

<!-- <script type="text/javascript" src="{{ asset('public/js/chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/angular-chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/ng-table.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/dirPagination.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/bootstrap-notify.min.js')}}"></script>-->

<script type="text/javascript" src="{{ asset('public/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/scopes/admin.js') }}"></script>

<script type="text/javascript">
  
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    //CKEDITOR.replace('.ckeditor');
    //bootstrap WYSIHTML5 - text editor
    //$('.textarea').wysihtml5();
  });
</script>

@yield('pagescript')
</body>
</html>
