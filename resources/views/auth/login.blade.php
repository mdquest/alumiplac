@extends('layouts.auth')

@section('content')

<div class="row">
<div class="col-md-6 col-md-offset-3">
  <div class="login-logo">
    <a href="#"><b>{{ config('app.name', 'Laravel') }}</b>&nbsp;Plataforma</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Efetuar login na plataforma</p>

    <form class="form-horizontal" method="POST" action="{{ route('login') }}" name="formUserLogin" id="formUserLogin">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus ng-model="user.email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Senha</label>

                            <div class="col-md-6">
                                <input id="password" type="password" ng-model="user.password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Lembrar usuário e senha
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-alumiplac" ng-disabled="formUserLogin.$invalid">
                                    Login
                                </button>

                                <a class="btn btn-alumiplac-link" href="{{ route('password.request') }}">
                                    Não lembra a senha?
                                </a>
                            </div>
                        </div>
                    </form>

    </div>
</div>
</div>
@endsection
