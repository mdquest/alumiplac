@extends('layouts.admin')

@section('content')

<section class="content-header">
      <h1>
        Cubic
        <small>Módulo de disparo de emails</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Configurações</a></li>
        <li class="active">Cubic</li>
      </ol>
    </section>

    <section class="content">
    <div class="callout callout-info">
        <h4>Atenção!</h4>
        Para fazer uso do sistema de disparo de emails é necessário baixar o app
        <a href="http://getbootstrap.com/javascript/#modals">nest link</a>
      </div>
      </section>

@endsection