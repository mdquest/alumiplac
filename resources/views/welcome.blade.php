@extends('layouts.app')

@section('content')

<section id="intro">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <div class="carousel-item active" style="background-image: url('01.jpg');">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Etiquetas de identificação</h2>
                <a href="#featured-services" class="btn-get-started scrollto">orçamento</a>
              </div>
            </div>
          </div>

          <div class="carousel-item" style="background-image: url({{ url('01.jpg') }});">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Painéis para máquinas</h2>
                <a href="#featured-services" class="btn-get-started scrollto">orçamento</a>
              </div>
            </div>
          </div>

          <div class="carousel-item" style="background-image: url('01.jpg');">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Brindes</h2>
                <a href="#featured-services" class="btn-get-started scrollto">orçamento</a>
              </div>
            </div>
          </div>

          <div class="carousel-item" style="background-image: url('01.jpg');">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Etiquetas de patrimônio</h2>
                <a href="#featured-services" class="btn-get-started scrollto">orçamento</a>
              </div>
            </div>
          </div>

         

        </div>

        <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- #intro -->

  <main id="main" ng-controller="IndexController">

       <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>Sobre nós</h3>
          <p>{{ $company->about }}</p>
        </header>

        <div class="row about-cols">

          <div class="col-md-4 wow fadeInUp">
            <div class="about-col">
              <div class="img">
                <img src="{{ asset('public/img/about-mission.jpg') }}" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Missão</a></h2>
              <p>{{ $company->mission }}</p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="about-col">
              <div class="img">
                <img src="{{ asset('public/img/about-plan.jpg') }}" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-list-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Visão</a></h2>
              <p>{{ $company->vision }}</p>
               
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
            <div class="about-col">
              <div class="img">
                <img src="{{ asset('public/img/about-vision.jpg') }}" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-eye-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Valores</a></h2>
              <p>{{ $company->values }}</p>
              
            </div>
          </div>

        </div>

      </div>
    </section><!-- #about -->



    <!--==========================
      Portfolio Section
    ============================-->
    <section id="portfolio"  class="section-bg" >
      <div class="container">

        <header class="section-header">
          <h3 class="section-title">Nossos produtos</h3>
        </header>

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">Todos</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container">

        @foreach($products as $product)
          <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp">
            <div class="portfolio-wrap">
              <figure>
                <img src="{{ url($product->image) }}" class="img-fluid" alt="">
                
                <a href="{{ url('produtos/'.$product->url) }}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
              </figure>

              <div class="portfolio-info">
                <h4><a href="{{ url($product->url) }}">{{ $product->name }}</a></h4>
                <p>{{ $product->name }}</p>
              </div>
            </div>
          </div>
        @endforeach
          </div>

        </div>

      </div>
    </section><!-- #portfolio -->


    <!--==========================
      Team Section
    ============================-->
       <!--==========================
      Contact Section
    ============================-->
    <section id="contact" class="section-bg wow fadeInUp">
      <div class="container">

        <div class="section-header">
          <h3>Contacte-nos</h3>
          <p>Nosso horário de funcionamento é de: Segunda a Quinta 7:30 às 17:30 e Sexta 7:30 às 16:30</p>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Endereço</h3>
              <address>Rua Bárbara Heliodora, 82 <br> Utinga,Santo André, SP</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Telefone</h3>
              <p><a href="tel:+155895548855">11 4461-1766</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:{{ $company->default_email }}">{{ $company->default_email }}</a></p>
            </div>
          </div>

        </div>

        <div class="form">
          
          <div id="sendmessage" ng-show="isMensagemEnviada">Sua mensagem foi enviada!</div>
          <div id="errormessage"></div>
          <form role="form" class="contactForm" method="POST"  ng-submit="processFormInsert()" name="formContact" id="formContact">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" ng-model="formData.name" placeholder="Seu nome" data-rule="minlen:4" data-msg="Please enter at least 4 chars"  required="required" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email"  ng-model="formData.email"  placeholder="Seu email" data-rule="email" data-msg="Please enter a valid email" required="required" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="title" id="title"  ng-model="formData.subject"  placeholder="Assunto" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message" ng-model="formData.content"></textarea>
              <div class="validation"></div>
            </div>
            <!-- ng-disabled="formContact.$invalid" -->
            <div class="text-center"><button type="submit" >Enviar mensagem</button></div>
          </form>
        </div>

      </div>
    </section><!-- #contact -->

  </main>


@endsection
