@extends('layouts.admin')

@section('content')

<style type="text/css">
  .inputfile {
  width: 0.1px;
  height: 0.1px;
  opacity: 0;
  overflow: hidden;
  position: absolute;
  z-index: -1;
}
</style>

<div class="" ng-controller="CompanyController">
<section class="content-header">
      <h1>
        Editar dados da empresa
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Configurações</a></li>
        <li><a href="#">Empresa</a></li>
      </ol>
    </section>


<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-4">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="img-responsive" id="company_image" name="company_image" src="{{ url('/') }}/<%company.image%>" alt="User profile picture">

              <hr>

              <h3 class="profile-username text-center"><%company.name%></h3>

              <p class="text-muted text-center"><%company.default_email%></p>

              <label for="myFile" class="btn btn-secondary btn-block">Selecionar</label>
              <input type="file" file-model="myFile" id="myFile" name="myFile" class="inputfile" ng-required required="required" />
              
              <a href="#" class="btn btn-primary btn-block" ng-click="uploadFile()" ><b>Salvar</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Extras</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i>Localização</strong>

              <p class="text-muted">
                <%company.place%>
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i>Região</strong>

              <p class="text-muted"><%company.region%></p>
           
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Dados gerais</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST"  ng-submit="processFormInsert()" name="formCompany" id="formCompany">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

              <div class="box-body">
                <div class="form-group">
                  <label for="name">Nome da empresa</label>
                  <input type="text" class="form-control" value="" id="name" name="name" placeholder="Nome da empresa" ng-model="company.name" >
                </div>
                <div class="form-group">
                  <label for="default_email">Email da empresa</label>
                  <input type="text" class="form-control" value="" id="default_email" placeholder="Nome da empresa" id="default_email" ng-model="company.default_email">
                </div>
                <div class="form-group">
                  <label for="mission">Missão</label>
                  <textarea class="form-control" placeholder="Missão" name="mission" id="mission" ng-model="company.mission"></textarea>
                </div>
                 <div class="form-group">
                  <label for="vision">Visão</label>
                  <textarea class="form-control" placeholder="Visão" name="vision" id="vision" ng-model="company.vision"></textarea>
                </div>
                 <div class="form-group">
                  <label for="values">Valores</label>
                  <textarea class="form-control" placeholder="Valores" name="values" id="values" ng-model="company.values"></textarea>
                </div>
                 <div class="form-group">
                  <label for="about">Conteúdo sobre</label>
                  <textarea class="form-control" name="about" id="about" ng-model="company.about" placeholder="Sobre a companhia"></textarea>
                </div>
                <!--<div class="form-group">
                  <label for="company.logo">Logo da empresa</label>
                  <input type="file" id="logo">

                  <p class="help-block"></p>
                </div>-->
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Salvar dados</button>
              </div>
            </form>
          </div>
        </div>
     </div>
 </section>
</div>
@endsection

@section('pagescript')

<script type="text/javascript" src="{{ asset('public/js/scopes/CompanyController.js') }}"></script>

<script type="text/javascript">
$("#myFile").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#company_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
</script>
@endsection