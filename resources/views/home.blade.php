@extends('layouts.admin')

@section('content')

  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

<section class="content">
      <!-- Small boxes (Stat box) -->
     <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-android-globe"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Newsletter</span>
              <span class="info-box-number">{{ $count_newletter }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa ion-social-usd"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Orçamentos</span>
              <span class="info-box-number">{{ $count_estimates }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-person"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Contatos</span>
              <span class="info-box-number">{{ count($contacts) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-star"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Novos membros</span>
              <span class="info-box-number">{{ count($users) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
        <div class="col-md-8">
            <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Últimos Orçamentos</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Cliente</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Material</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($estimates as $estimate)
                  <tr>
                    <td><a href="pages/examples/invoice.html">{{ $estimate->name }}</a></td>
                    <td>{{ $estimate->email }}</td>
                    <td><span class="label {{ $estimate->answered_times > 0?'label-success':'label-warning'  }}">{{ $estimate->answered_times > 0 ? 'respondido' : 'não respondido'   }}</span></td>
                    <td>
                     {{ $estimate->material }}
                    </td>
                  </tr>
                  @endforeach
                  
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="{{ route('home_mail_contact') }}?filter=estimates" class="btn btn-sm btn-default btn-flat pull-right">Ver todos os orçamentos</a>
            </div>
            <!-- /.box-footer -->
          </div>

           <!-- quick email widget -->
           <div  ng-controller="QuickMailController">
           <form method="post" ng-submit="processFormInsert()" name="formQuickMail" id="formQuickMail">
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Email Rápido</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
              
                <div class="form-group">
                  <input type="email" class="form-control" name="email" id="email" ng-model="formData.email" placeholder="Email to:">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" ng-model="formData.subject" placeholder="Subject">
                </div>
                <div>
                  <textarea class="textarea" placeholder="Message" name="content" id="content" ng-model="formData.content" 
                            style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
              
            </div>
            <div class="box-footer clearfix">
              <button type="submit" class="pull-right btn btn-default" id="sendEmail">Send
                <i class="fa fa-arrow-circle-right"></i></button>
            </div>
          </div>
          </form>
          </div>

        </div>
        <div class="col-md-4">
        <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Últimos Usuários</h3>

                  <div class="box-tools pull-right">
                    <span class="label label-danger">{{ count($newst_users) }}</span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <ul class="users-list clearfix">
                    @foreach($newst_users as $user_new)
                    <li>
                      <img src="{{ url($user_new->image) }}" alt="User Image">
                      <a class="users-list-name" href="#">{{ \App\Helpers\StringHelper::instance()->get_first_name($user_new->name) }}</a>
                      <span class="users-list-date"></span>
                    </li>
                    @endforeach
                    
                  </ul>
                  <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="javascript:void(0)" class="uppercase">Ver todos os usuários</a>
                </div>
                <!-- /.box-footer -->
              </div>

        </div>
      </div>
</section>

@endsection
