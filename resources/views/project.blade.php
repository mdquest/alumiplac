@extends('layouts.admin')

@section('content')

<section class="content-header">
      <h1>
        Sobre este projeto
        <small>Cubic #007612</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Invoice</li>
      </ol>
    </section>

<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-group"></i>&nbsp;SGPA - Grupo Expressão
            <small class="pull-right">Data: 18/01/2018</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          Desenvolvimento
          <address>
            <strong>Marco Aurélio Lima.</strong><br>
             <i class="fa fa-envelope"></i>&nbsp;marckdx@outlook.com<br>
             <i class="fa fa-github"></i>&nbsp;<a href="http://github.com/marckdx" class="text-black">@marckdx</a><br>
             <i class="fa fa-phone"></i>&nbsp;(13) 98109 4830<br>
             <i class="fa fa-globe"></i>&nbsp;<a href="https://marckdx.github.io/?utm_source=alumiplac.com.br&utm_medium=linkredirect&utm_campaign=client-campaign&utm_term=etiquetas" target="_blank">marckdx.github.io</a><br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Análise
          <address>
            <strong>Saul Feitosa Pinto</strong><br>
             <i class="fa fa-envelope"></i>&nbsp;saul.expressao@gmail.com<br>
             <i class="fa fa-phone"></i>&nbsp;(13) 7805 4266<br>
             <i class="fa fa-globe"></i>&nbsp;<a href="http://www.expressaofotoevideo.com.br/" target="_blank">expressaofotoevideo.com.br</a><br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Grupo Expressão</b><br>
          <br>
          <b>Suporte:</b> <a href="mailto:ewstestesite@gmail.com">ewstestesite@gmail.com</a><br>
          <b>Backlog:</b>&nbsp;<a href="">#082018</a><br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <hr>
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Agradecimentos:</p>

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Desenvolvido com Laravel, Angular e MySQL. Agradecimentos à Almsaeed Studios e demais colaboradores que contribuiram direta e indiretamente com este projeto.
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Detalhes do sistema</p>

          <div class="table-responsive">
            <table class="table">
              <tbody><tr>
                <th style="width:50%">Versão:</th>
                <td>2.4.0</td>
              </tr>
              <tr>
                <th>ID:</th>
                <td>#007612</td>
              </tr>
              <tr>
                <th>Cliente:</th>
                <td>Alumiplac</td>
              </tr>
              <tr>
                <th>Interface:</th>
                <td>Lite</td>
              </tr>
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="https://bitbucket.org/mdquest/alumiplac/wiki/Home" target="_blank" class="btn btn-default"><i class="fa fa-github"></i> Fork</a>
          <a type="button" href="http://expressaofotoevideo.com.br" target="_blank" class="btn btn-success pull-right"><i class="fa fa-file"></i>&nbsp;Contato
          </a>
          <a type="button" href="{{ url('manual.pdf') }}" target="_blank" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i>&nbsp;Manual
          </a>
        </div>
      </div>
    </section>
@endsection