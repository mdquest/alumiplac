@extends('layouts.app')

@section('content')

 <main id="main" style="margin-top: 60px;" ng-controller="EstimateController">
<section id="contact" class="section-bg wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
      <div class="container">
        <header class="section-header">
          <h3>Orçamento</h3>
          <p>Faça seu orçamento sem compromisso conosco!</p>
        </header>
 



<form role="form" class="contactForm form" method="POST" ng-submit="processFormInsert()" name="formContactEstimate" id="formContactEstimate">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" ng-model="contact.name" placeholder="Seu nome" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="required">
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" ng-model="contact.email" placeholder="Seu email" data-rule="email" data-msg="Please enter a valid email" required="required">
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="title" id="title" ng-model="contact.subject" placeholder="Assunto" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="title" id="title" ng-model="contact.business_name" placeholder="Nome da empresa" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <select class="form-control" name="title" id="title" ng-model="estimate.material_type_id" placeholder="Ma" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
              	@foreach($materials as $material)
              		<option value="{{ $material->id }}">{{ $material->name }}</option>
              	@endforeach
              </select>
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <input type="number" class="form-control" name="title" id="title" ng-model="estimate.quantity" placeholder="Quantidade" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="title" id="title" ng-model="estimate.width" placeholder="Largura (mm)" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
              <div class="validation"></div>
            </div>
             <div class="form-group">
              <select class="form-control" name="title" id="title" ng-model="estimate.height" placeholder="Espessura" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
              	<option>0.3 mm</option>
              	<option>0.4 mm</option>
              	<option>0.5 mm</option>
              	<option>0.7 mm</option>
              	<option>1.0 mm</option>
              	<option>1.2 mm</option>
              	<option>1.5 mm</option>
              	<option>2.0 mm</option>
              	<option>3.0 mm</option>
              </select>
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <select class="form-control" name="title" id="title" ng-model="estimate.fix_type_id" placeholder="Espessura" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
              	@foreach($fix_types as $fix_type)
              		<option value="{{ $fix_type->id }}">{{ $fix_type->name }}</option>
              	@endforeach
              	
              </select>
              <div class="validation"></div>
            </div>
            <div class="form-group" ng-show="estimate.fix_type_id == 1">
              <input type="text" class="form-control" name="title" id="title" ng-model="estimate.holes_size" placeholder="Diâmetro dos furos" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
              <div class="validation"></div>
            </div>
            <div class="form-group" ng-show="estimate.fix_type_id == 1">
              <input type="text" class="form-control" name="title" id="title" ng-model="estimate.qt_holes" placeholder="Quantidade de furos" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message" ng-model="contact.content"></textarea>
              <div class="validation"></div>
            </div>
            <div class="form-group">
            	<input type="file" class="form-control" name="file" ng-model="file">
            </div>
            <!-- ng-disabled="formContact.$invalid" -->
            <div class="text-center"><button type="submit">Enviar mensagem</button></div>
          </form>
         </div>
             </div>
  </section>
</main>
@endsection

@section('pagescript')
<script type="text/javascript" src="{{ asset('public/js/scopes/EstimateController.js') }}"></script>
@endsection