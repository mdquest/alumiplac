@extends('layouts.app')

@section('content')

  <main id="main" style="margin-top: 60px;">

  <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>{{ $db_page->name}}</h3>
          <p>{{ $db_page->description }}</p>
        </header>

        @if(count($gallery) > 0)
        <div class="row portfolio-container" style="position: relative; height: 360px;">
          @foreach($gallery as $page)          
          <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp" style="position: absolute; left: 0px; top: 0px; visibility: visible; animation-name: fadeInUp;">
            <div class="portfolio-wrap">
            	<img src="{{ url('/storage/app/'.$page->location) }}" class="img-fluid">
            </div>
           </div>
           @endforeach

           </div>
          </div>
          @endif

        </div>
  </main>
@endsection