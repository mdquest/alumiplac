@extends('layouts.admin')

@section('content')
<section class="content-header">
      <h1>
        Editar página
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('home_pages_index') }}">Páginas</a></li>
        <li><a href="#">Editar</a></li>
      </ol>
    </section>

<section class="content" ng-controller="PageController" ng-init="loadPage()">
      <input type="hidden" name="page_id" id="page_id" value="{{ $page_id }}">
      <div class="row">
      	<div class="col-md-2">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Tipos</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="{{ url('/home/pages/edit/'.$page_id) }}"><i class="fa fa-inbox"></i> Gerais</a></li>
                <li><a href="{{ url('/home/pages/gallery/'.$page_id) }}"><i class="fa fa-envelope-o"></i>Imagens</a></li>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
      	</div>

        <div class="col-md-10">

        <div class="box box-info">
            <div class="box-header with-border">

            </div>
            <div class="box-body">
          <img src="{{ url('/') }}/<%page.image%>" id="page_image" alt="Imagem da página, exibida em produtos" style="width: 250px;"/>
          <div class="form-group" style="display: none;">
                  <label for="name">Imagem</label>
                  <input type="file" onchange="readURL(this);" class="form-control" value="" id="inputImage" file-model="myFile" ng-model="myFile" name="inputImage" placeholder="Nome da empresa">
                </div>
                <div class="form-group" style="margin-top: 20px;">
                  <button class="btn btn-success" id="buttonOpenFile"><i class="fa fa-folder"></i>&nbsp;selecionar</button>&nbsp;
                  <button class="btn btn-primary" ng-click="uploadImage()"><i class="fa fa-send"></i>&nbsp;enviar</button>
                </div>
              </div>
          </div>



        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><%page.name%></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
      <div class="box-body">
              <div class="table-responsive">
              <form role="form" method="POST"  ng-submit="processFormUpdate()" name="formUpdatePage" id="formUpdatePage">
                <input type="hidden" name="page_id" id="page_id" bg-model="page.id" value="{{ $page_id }}">

               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <div class="form-group">
                  <label for="name">Página</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="Nome da empresa" ng-model="page.name" >
                </div>
                 <div class="form-group">
                  <label for="name">URL</label>
                  <div class="input-group">
                  <div class="input-group-addon">{{ url('/') }}/<% page.is_product?"produtos/":"" %></div>
                  <input type="text" class="form-control" value="" id="name" name="name" placeholder="Url do produto" ng-model="page.url" >
                  </div>
                </div>
                 <div class="form-group">
                  <label for="name">Conteúdo</label>
                  <textarea type="text" class="form-control" value="" id="name" name="name" placeholder="Texto exibido no topo da página" ng-model="page.content" ></textarea>
                </div>
                 <div class="form-group">
                  <label for="name">Palavras-chave</label>
                  <input type="text" class="form-control" value="" id="name" name="name" placeholder="Meta Keywords" ng-model="page.keywords" >
                </div>
                <div class="form-group">
                  <label for="name">Descrição</label>
                  <input type="text" class="form-control" value="" id="name" name="name" placeholder="Meta Description" ng-model="page.description" >
                </div>
                <div class="form-group">
                  
                  <input type="checkbox"  id="name" name="name" placeholder="Meta Description" ng-model="page.is_blog" >&nbsp;<label for="name"> É uma página de Blog</label>
                </div>
                <div class="form-group">
                  
                  <input type="checkbox" id="name" name="name" placeholder="Meta Description" ng-model="page.is_product" ><label for="name">É uma página de produto</label>
                </div>
                <div class="form-group">
                <input type="submit" class="btn btn-primary pull-right">
                </div>
               
                </form>
              </div>
        </div>
      </div>
    </div>
  </div>
  </section>

  
  @endsection


@section('pagescript')
<script type="text/javascript" src="{{ asset('public/js/scopes/PageController.js') }}"></script>
@endsection