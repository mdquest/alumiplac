@extends('layouts.admin')

@section('content')
<section class="content-header">
      <h1>
        Editar página
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('home_pages_index') }}">Páginas</a></li>
        <li><a href="#">Imagens</a></li>
      </ol>
    </section>

<section class="content" ng-controller="PageFileController" ng-init="loadPage()">
      <input type="hidden" name="page_id" id="page_id" value="{{ $page_id }}">
      <div class="row">
      	<div class="col-md-2">

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Tipos</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="{{ url('home/pages/edit/'.$page_id) }}"><i class="fa fa-inbox"></i> Gerais</a></li>
                <li class="active"><a href="{{ url('home/pages/gallery/'.$page_id) }}"><i class="fa fa-envelope-o"></i> Imagens</a></li>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
      	</div>

  <div class="col-md-10">
    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Imagens</h3>

              <div class="box-tools pull-right">
                <form>
                  <input type="hidden" name="gallery_id" id="gallery_id" value="" />
                  <input type="file" name="myFile" file-model="myFile" id="myFile" ng-model="myFile" style="display: none;" /> 

                  <button class="btn btn-default" ng-click="loadAll()" id="reloadAll">recarregar</button>
                  <button class="btn-primary btn py-5" id="buttonOpenFile" name="buttonOpenFile"><i class="fa fa-folder"></i>&nbsp;selecionar</button>
                  <button id="btnSaveFile" disabled="disabled" class="btn-success btn py-5" ng-click="uploadFile()"><i class="fa fa-send"></i>&nbsp;enviar</button>
                </form>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              

              <ul class="mailbox-attachments clearfix">
               
                
                <li ng-repeat="image in gallery">
                  <span class="mailbox-attachment-icon has-img"><img src="{{ url('/storage/app') }}/<%image.location%>" alt="Attachment" style="max-height: 100px;"></span>

                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"><i class="fa fa-camera"></i>&nbsp;<%image.id%></a>
                        <span class="mailbox-attachment-size">
                          2.67 MB
                          <a href="#" ng-click="deleteFile(image.id)" class="btn btn-default btn-xs pull-right"><i class="fa fa-times"></i></a>
                        </span>
                  </div>
                </li>
                
              </ul>
            </div>
            </div>
          </div>
      </div>
    </section>



  <div class="modal fade" tabindex="-1" role="dialog" id="modalMessage">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="messageTitle">Status da operação</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">continuar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  @endsection


@section('pagescript')
<script type="text/javascript" src="{{ asset('public/js/scopes/PageFileController.js') }}"></script>
@endsection
