@extends('layouts.admin')

@section('content')

<section class="content-header">
      <h1>
        Páginas
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('home_pages_index') }}">Páginas</a></li>
      </ol>
    </section>

<section class="content" ng-controller="PageController">

      <div class="row">
        <div class="col-md-8 col-sm-12 col-md-offset-2">
        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Páginas</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <a href="#" data-toggle="modal" data-target="#modalInsertPage" class="btn btn-primary btn-block margin-bottom">criar página</a>

              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Página</th>
                    <th>URL</th>
                    <th>Criação</th>
                    <th>Tipo d epágina</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr ng-repeat="page in pages">
                    <td><a href="{{ url('home/pages/edit/') }}/<%page.id%>"><%page.name%></a></td>
                    <td><a href="{{ url('/') }}/<%page.is_product?'produto/':'' %><%page.url%>" class="text-black"><%page.is_product?'produtos/':'' %><%page.url%></a></td>
                    <td><%page.user_name%></td>
                    <td><span class="label label-primary"><%page.is_product?'produto':'outra' %></span></td>
                    <td><button class="btn btn-default btn-sm" ng-click="deletePage(page.id)"><i class="fa fa-times"></i></button></td>
                    <td><a href="{{ url('home/pages/edit/') }}/<%page.id%>" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <!-- /.box-footer -->
          </div>
        </div>
      </div>

      <form name="createPage" id="createPage" ng-submit="processFormInsert()">
      <input type="hidden" value="{{ csrf_token() }}" id="_token" name="_token">
      <div class="modal fade" tabindex="-1" role="dialog" id="modalInsertPage" name="modalInsertPage">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Nova página</h4>
            </div>
            <div class="modal-body">
               <div class="form-group">
                  <label for="name">Página</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="Nome da página" ng-model="page.name" >
                </div>
                 <div class="form-group">
                  <label for="name">URL</label>
                  <div class="input-group">
                  <div class="input-group-addon">{{ url('/') }}/<% page.is_product?"produtos/":"" %></div>
                  <input type="text" class="form-control" value="" id="name" name="name" placeholder="Url do produto / página" ng-model="page.url" >
                  </div>
                  <div class="form-group">
                  
                  <input type="checkbox" id="is_product" name="is_product" placeholder="Meta Description" ng-model="page.is_product" ><label for="is_product">É uma página de produto</label>
                </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
              <button type="submit" class="btn btn-primary">Criar</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
    </form>




  </section>

@endsection


@section('pagescript')
<script type="text/javascript" src="{{ asset('public/js/scopes/PageController.js') }}"></script>
@endsection
