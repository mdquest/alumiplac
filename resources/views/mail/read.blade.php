@extends('layouts.admin')

@section('content')

<section class="content" ng-controller="ReadMailController">
      <div class="row">
        <div class="col-md-3">
          <!--<a href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a>-->

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Diretórios</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="{{ route('home_mail_contact') }}"><i class="fa fa-inbox"></i> Todos
                  <span class="label label-primary pull-right">{{ $countContacts }}</span></a></li>
                <li><a href="#"><i class="fa fa-envelope-o"></i> Respondidos</a></li>
                <li><a href="#"><i class="fa fa-file-text-o"></i> Em alerta</a></li>
                <li><a href="#"><i class="fa fa-filter"></i> Excluídos</a>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Tipos</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="{{ route('home_mail_contact') }}?filter=estimates"><i class="fa fa-circle-o text-red"></i> Orçamentos</a></li>
                <li><a href="{{ route('home_mail_contact') }}?filter=contacts"><i class="fa fa-circle-o text-yellow"></i> Contatos</a></li>
                <li><a href="{{ route('home_mail_contact') }}"><i class="fa fa-circle-o text-light-blue"></i> Todos</a></li>
                </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-md-9" id="emailMessageDiv" name="emailMessageDiv">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Ler mensagem de contato</h3>

              <div class="box-tools pull-right">
                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Previous"><i class="fa fa-chevron-left"></i></a>
                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Next"><i class="fa fa-chevron-right"></i></a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3>{{ $contact->subject }}</h3>
                <h5><b>De:</b> {{ $contact->email }}
                  <span class="mailbox-read-time pull-right">{{ $contact->created_at }}</span></h5>
              </div>
              <!-- /.mailbox-read-info -->
              <div class="mailbox-controls with-border text-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="" data-original-title="Delete">
                    <i class="fa fa-trash-o"></i></button>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="" data-original-title="Reply">
                    <i class="fa fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="" data-original-title="Forward">
                    <i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="" data-original-title="Print">
                  <i class="fa fa-print"></i></button>
              </div>
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                {{ $contact->content }}
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            @if(count($files) > 0)
            <div class="box-footer">
              <ul class="mailbox-attachments clearfix">

              	@foreach($files as $file)
                <li>
                  <span class="mailbox-attachment-icon"><i class="{{ \App\Helpers\StringHelper::instance()->file_name_to_icon($file->name) }}"></i></span>

                  <div class="mailbox-attachment-info">
                    <a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>{{ $file->name }}</a>
                        <span class="mailbox-attachment-size">
                          1,245 KB
                          <a href="{{ url('storage/app/'.$file->location) }}" target="_blank" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                        </span>
                  </div>
                </li>
                @endforeach
              </ul>
            </div>
            @endif
            <!-- /.box-footer -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="button" class="btn btn-default"><i class="fa fa-reply"></i> Responder</button>
                <button type="button" class="btn btn-default"><i class="fa fa-share"></i> Enviar</button>
              </div>
              <button type="button" ng-click="deleteEmail({{ $contact->id }})" class="btn btn-default"><i class="fa fa-trash-o"></i> Deletar</button>
              <a type="button" class="btn btn-default" href="{{ url('home/mail/contact/print/'.$contact->id) }}"><i class="fa fa-print"></i> Imprimir</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>

       </div>
     </section>
@endsection

@section('pagescript')
<script type="text/javascript" src="{{ asset('public/js/jquery.printElement.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/scopes/MailController.js') }}"></script>
@endsection
