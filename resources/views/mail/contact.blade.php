@extends('layouts.admin')

@section('content')

<section class="content" ng-controller="MailController">
  <input type="hidden" name="filter" id="filter" value="{{ $filter }}">
      <div class="row">
        <div class="col-md-3">
          <!--<a href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a>-->

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Diretórios</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"><i class="fa fa-inbox"></i> Todos
                  <span class="label label-primary pull-right">{{ $countContacts }}</span></a></li>
                <li><a href="#"><i class="fa fa-envelope-o"></i> Respondidos</a></li>
                <li><a href="#"><i class="fa fa-file-text-o"></i> Em alerta</a></li>
                <li><a href="#"><i class="fa fa-filter"></i> Excluídos</a>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Tipos</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="{{ route('home_mail_contact') }}?filter=estimates"><i class="fa fa-circle-o text-red"></i> Orçamentos</a></li>
                <li><a href="{{ route('home_mail_contact') }}?filter=contacts"><i class="fa fa-circle-o text-yellow"></i> Contatos</a></li>
                <li><a href="{{ route('home_mail_contact') }}"><i class="fa fa-circle-o text-light-blue"></i> Todos</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Inbox</h3>

              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Pesquisar" ng-model="search" id="criteria" name="criteria">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <!--<button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                </div>-->
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                  <%page%>/<%pages%>
                  <div class="btn-group">
                    <button type="button" ng-click="previousPage()" ng-disabled="page == 1" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" ng-click="nextPage()" ng-disabled="page == pages" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                 
                  <tr ng-repeat="contact in contacts">
                    <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;" ><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></td>
                    <td class="mailbox-star"><a href="#" ng-show="contact.qt_estimate > 0"><i class="fa fa-star text-yellow"></i></a></td>
                    <td class="mailbox-name"><a href="{{ url('home/mail/contact/read/') }}/<%contact.id%>"><%contact.email%></a></td>
                    <td class="mailbox-subject"><b><%contact.title%></b>
                    </td>
                    <td class="mailbox-attachment"></td>
                    <td class="mailbox-date"><%contact.created_at%></td>
                  </tr>
                  
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <!--<button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                </div>-->
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                  <%page%>/<%pages%>
                  <div class="btn-group">
                    <button type="button"  ng-click="previousPage()" ng-disabled="page == 1"  class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                    <button type="button"   ng-click="nextPage()" ng-disabled="page == pages" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
            </div>
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


@endsection

@section('pagescript')
<script type="text/javascript" src="{{ asset('public/js/scopes/MailController.js') }}"></script>
@endsection
