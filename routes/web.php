<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index_index')->name('index_index');

Route::get('/index', function () {
    return redirect()->route('index_index');
});

Route::get('/orcamento', 'EstimateController@app_estimate_index')->name('app_estimate_index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/about-this-project', 'HomeController@about_this_project')->name("about_this_project");

Route::get('/home/settings/company', 'SettingsController@home_settings_company')->name('home_settings_company');
Route::get('/home/settings/analytics', 'SettingsController@home_settings_analytics')->name('home_settings_analytics');
Route::get('/api/settings/company', 'SettingsController@admin_settings_company')->name('admin_settings_company');

Route::get('/home/pages', 'PagesController@home_pages_index')->name('home_pages_index');
Route::get('/home/mail/contact', 'MailController@home_mail_contact')->name('home_mail_contact');
Route::get('/home/mail/contact/read/{id}', 'MailController@home_mail_contact_read')->name('home_mail_contact_read');
Route::get('/home/cubic', 'CubicController@home_cubic_index')->name('home_cubic_index');

Route::get('produtos/{page}', 'PagesController@home_pages_handler')->name('home_pages_handler');
Route::get('uploads/{file}', 'FileController@home_get_file')->name('home_get_file');

Route::get('/home/mail/contact/print/{id}', 'MailController@mail_contact_print')->name('mail_contact_print');
Route::get('/api/mail/contact/{filter}/{page}', 'MailController@api_mail_contact')->name('api_mail_contact');


Route::post('/api/settings/company/save', 'SettingsController@save_settings_company')->name('save_settings_company');
Route::post('/api/settings/company/save/logo', 'SettingsController@save_settings_company_logo')->name('save_settings_company_logo');
Route::post('/api/mail/contact/create', 'MailController@admin_mail_contact_create')->name('admin_mail_contact_create');
Route::post('/api/mail/newsletter/create', 'MailController@admin_newsletter_create')->name('admin_newsletter_create');
Route::post('/api/mail/create', 'MailController@admin_email_create')->name('admin_email_create');
Route::get('/api/cubic/load/{api_key}', 'CubicController@api_cubic_load')->name('api_cubic_load');

Route::get('/api/pages/getall', 'PagesController@api_get_pages')->name('api_get_pages');
Route::get('/home/pages/create', 'PagesController@home_create_page')->name('home_create_page');
Route::get('/home/pages/edit/{id}', 'PagesController@home_edit_page')->name('home_edit_page');
Route::get('/api/pages/get/{id}', 'PagesController@api_edit_page')->name('api_edit_page');
Route::get('/api/pages/gallery/get/{id}', 'PagesController@api_gallery_page')->name('api_gallery_page');
Route::get('/api/pages/gallery/delete/{id}', 'PagesController@api_gallery_delete')->name('api_gallery_delete');
Route::post('/api/file/upload', 'FileController@save_file')->name('save_file');
Route::get('/api/gallery/save/{file_id}/{page_id}', 'PagesController@save_on_gallery')->name('save_on_gallery');
Route::get('/home/pages/gallery/{id}', 'PagesController@home_gallery_page')->name('home_gallery_page');
Route::post('/api/pages/image/save/{id}', 'PagesController@api_update_page_image')->name('api_update_page_image');
Route::post('/api/pages/save/{id}', 'PagesController@api_pages_save')->name('api_pages_save');
Route::post('/api/pages/create', 'PagesController@api_pages_create')->name('api_pages_create');
Route::get('/api/pages/delete/{id}','PagesController@api_pages_delete')->name('api_pages_delete');

