angular.module("appAlumiplac").controller("MailController", function($scope, $http){
	$scope.contacts = [{}];
	$scope.page = 1;
	$scope.pages = 0;
	$scope.itemsPerPage = 12;
	$scope.begin = 0;
	$scope.end = 12;

	var loadContacts = function($page){
		var filter = $("#filter").val();

		var json_url = base_url + "/api/mail/contact/" + filter + "/" + ($page-1);
		$http.get(json_url).success(function($data){
			$scope.contacts = $data.contacts;
			console.log($data);
			$scope.pages = $data.pages;
			$scope.itemsPerPage = $data.itemsPerPage;
			$scope.begin = $data.begin;
			$scope.end = $data.end;
		});	
	};

	loadContacts($scope.page, );

	$scope.previousPage = function()
	{
		$scope.page--;
		loadContacts($scope.page);

	}

	$scope.nextPage = function()
	{
		$scope.page++;
		loadContacts($scope.page);
	}
});

angular.module("appAlumiplac").controller("ReadMailController", function($scope, $http){
	$scope.deleteEmail = function($id)
	{
		
	}

	$scope.printEmail = function()
	{
		var printContents = document.getElementById("emailMessageDiv").innerHTML;
	     var originalContents = document.body.innerHTML;
	     document.body.innerHTML = printContents;
	     window.print();
	     document.body.innerHTML = originalContents;
	}
});

function printDiv(divName) {
     
}