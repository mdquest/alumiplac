angular.module("appAlumiplac").controller("EstimateController", function($scope, $http){
	$scope.contact = null;
	$scope.files = [{}];
	$scope.estimate = null;

	$scope.processFormInsert = function()
	{

	};

	$scope.uploadFile = function()
	{
		var file = $scope.myFile;
		var uploadUrl = base_url+"/api/settings/company/save/logo";
		var text = null;
		$data = fileUpload.uploadFileToUrl(file, uploadUrl, text);
	};
});

angular.module("appAlumiplac").directive('fileModel', ['$parse', function ($parse) {
    return {
    restrict: 'A',
    link: function(scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;

        element.bind('change', function(){
            scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
            });
        });
    }
   };
}]);