angular.module("appAlumiplac").controller("CompanyController", function($scope, $http, fileUpload){
	$scope.company = {};

	var loadCompanyData = function(){
		var json_url = base_url + "/api/settings/company";
		$http.get(json_url).success(function($data){
			$scope.company = $data;
		});	
	};

	loadCompanyData();

	$scope.processFormInsert = function()
	{
		var postUrl = base_url+"/api/settings/company/save";

		$http({
			  method  : 'POST',
			  url     : postUrl,
			  data    : $.param($scope.company), 
			  headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
			 }).success(function($data) {
			 	if($data.inserted){
    				$("#modalMessage").modal();
    			}else{
    				$("#modalErrorMessage").modal();
    			}
    		});

	};

	$scope.uploadFile = function()
	{
		var file = $scope.myFile;
		var uploadUrl = base_url+"/api/settings/company/save/logo";
		var text = null;
		$data = fileUpload.uploadFileToUrl(file, uploadUrl, text);

	}
});

angular.module("appAlumiplac").directive('fileModel', ['$parse', function ($parse) {
    return {
    restrict: 'A',
    link: function(scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;

        element.bind('change', function(){
            scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
            });
        });
    }
   };
}]);

// We can write our own fileUpload service to reuse it in the controller
angular.module("appAlumiplac").service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl, name){
         var fd = new FormData();
         fd.append('file', file);
         fd.append('name', name);
         $http.post(uploadUrl, fd, {
             transformRequest: angular.identity,
             headers: {'Content-Type': undefined,'Process-Data': false}
         })
         .success(function($data){
         	$("#company_image").attr("src", base_url + $data.file_url);
         	$("#modalMessage").modal();
         	return $data;
         })
         .error(function(){
            console.log("Error");
            $("#modalErrorMessage").modal();
            return null;
         });
     }
 }]);