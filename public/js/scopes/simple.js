angular.module("appAlumiplac",  []);

angular.module('appAlumiplac').config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%');
  $interpolateProvider.endSymbol('%>');
});

var base_url = $("#base_url").val();

angular.module("appAlumiplac").controller("IndexController", function($scope, $http){
	$scope.isMensagemEnviada = false;
	 $scope.formData = {};
	 $scope.sent = false;

	$scope.processFormInsert = function()
	{
		var postUrl = base_url+"/api/mail/contact/create";

		$http({
			  method  : 'POST',
			  url     : postUrl,
			  data    : $.param($scope.formData), 
			  headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
			 }).success(function($data) {
			 	$scope.isMensagemEnviada = true;
    			$scope.sent = $data.sent;
    			$scope.formData = {};
    			console.log($data);
    		});

	};
});


angular.module("appAlumiplac").controller("NewsLetterController", function($scope, $http){
	$scope.isMensagemEnviada = false;
	 $scope.formData = {};
	 $scope.inserted = false;

	$scope.processFormInsert = function()
	{
		var postUrl = base_url+"/api/mail/newsletter/create";

		$http({
			  method  : 'POST',
			  url     : postUrl,
			  data    : $.param($scope.formData), 
			  headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
			 }).success(function($data) {
			 	$scope.isMensagemEnviada = true;
    			$scope.inserted = $data.inserted;
    			$scope.formData = {};
    			console.log($data);
    		});

	};
});