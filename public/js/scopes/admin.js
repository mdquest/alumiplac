angular.module("appAlumiplac",  []);

angular.module('appAlumiplac').config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%');
  $interpolateProvider.endSymbol('%>');
});

var base_url = $("#base_url").val();

function messageBoxOk($message, $title)
{
	$("#modalMessageTitle").html($title);
	$("#modalMessageBody").html($message);
	$("#modalMessageButtonOk").addClass("btn-primary");
	$("#modalMessageButtonOk").removeClass("btn-danger");
	$("#modalMessage").modal('show');
}

function messageBoxError($message, $title)
{	
	$("#modalMessageTitle").html($title);
	$("#modalMessageBody").html($message);
	$("#modalMessageButtonOk").addClass("btn-danger");
	$("#modalMessageButtonOk").removeClass("btn-primary");
	$("#modalMessage").modal('show');
}

angular.module("appAlumiplac").controller("QuickMailController", function($scope, $http){
	$scope.isMensagemEnviada = false;
	$scope.formData = {};
	$scope.inserted = false;

	$scope.processFormInsert = function()
	{
		var postUrl = base_url+"/api/mail/create";

		$http({
			  method  : 'POST',
			  url     : postUrl,
			  data    : $.param($scope.formData), 
			  headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
			 }).success(function($data) {
			 	console.log($data);

			 	if($data.sent == true){
			 		$scope.formData = null;
    				$("#modalMessage").modal();
    			}else{
    				$("#modalErrorMessage").modal();
    			}
    		});

	};
});

