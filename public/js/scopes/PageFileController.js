$("#buttonOpenFile").on("click", function(){
	$("#btnSaveFile").prop('disabled', false);
	$('#myFile').trigger('click'); 
});

angular.module("appAlumiplac").controller("PageFileController", function($scope, $http, fileUpload){
	$scope.pages = [{}];
	$scope.gallery = [{}];
	$scope.page = null;
	$scope.myFile = null;


	var loadPage = function()
	{
		var id = $("#page_id").val();
		var json_url = base_url + "/api/pages/get/" + id;

		$http.get(json_url).success(function($data){
			$scope.page = $data;
		});
	};

	var loadGallery = function()
	{
		var id = $("#page_id").val();
		var json_url = base_url + "/api/pages/gallery/get/" + id;

		$http.get(json_url).success(function($data){
			$scope.gallery = $data;
		});		
	};



	$scope.gerais = function()
	{
		$scope.show_tab = "gerais";
	};

	$scope.galeria = function()
	{
		$scope.show_tab = "galeria";
	};

	$scope.carrossel = function()
	{
		$scope.show_tab = "carrossel";
	};

	$scope.deleteFile = function(id)
	{
		var json_url = base_url + "/api/pages/gallery/delete/" + id;

		$http.get(json_url).success(function($data){
			if($data.deleted)
			{
				loadGallery();
			}
		});		
	};

	$scope.uploadFile = function()
	{
		$("#btnSaveFile").prop('disabled', true);
		var file = $scope.myFile;
		var uploadUrl = base_url+"api/file/upload";
		var text = null;
		$data = fileUpload.uploadFileToUrl(file, uploadUrl, text);
		loadGallery();

		$("#modalMessage").modal('show');
	};

	$scope.loadAll = function()
	{
		loadGallery();
	};

	loadPage();
	loadGallery();

});

angular.module("appAlumiplac").directive('fileModel', ['$parse', function ($parse) {
    return {
    restrict: 'A',
    link: function(scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;

        element.bind('change', function(){
            scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
            });
        });
    }
   };
}]);

angular.module("appAlumiplac").service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl, name){
         var fd = new FormData();
         fd.append('file', file);
         fd.append('name', name);
         $http.post(uploadUrl, fd, {
             transformRequest: angular.identity,
             headers: {'Content-Type': undefined,'Process-Data': false}
         })
         .success(function($data){

         	var page_id = $("#page_id").val();
			var url_gallery = base_url + "/api/gallery/save/"+ $data.id+"/" + page_id;

			$http.get(url_gallery).success(function($data){
				$('#reloadAll').trigger('click'); 
				return $data;
			});

         })
         .error(function(){

            return null;
         });
     }
 }]);