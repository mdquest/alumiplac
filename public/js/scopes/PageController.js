$("#buttonOpenFile").on("click", function(){
	$('#inputImage').trigger('click'); 
});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#page_image')
        .attr('src', e.target.result)
        .width(200)
        .height(150);
    };
    reader.readAsDataURL(input.files[0]);
  }
}

angular.module("appAlumiplac").controller("PageController", function($scope, $http, fileUpload){
	$scope.pages = [{}];
	$scope.gallery = [{}];
	$scope.page = null;
	$scope.show_tab = "gerais";
	$scope.myFile = null;

	var loadPages = function()
	{
		var json_url = base_url + "/api/pages/getall";

		$http.get(json_url).success(function($data){
			$scope.pages = $data;
		});
	};

	var loadPage = function()
	{
		var id = $("#page_id").val();
		var json_url = base_url + "/api/pages/get/" + id;

		$http.get(json_url).success(function($data){
			$scope.page = $data;
		});
	};

	$scope.processFormInsert = function()
	{
		var json_url = base_url+"/api/pages/create";

		$http({
			  method  : 'POST',
			  url     : json_url,
			  data    : $.param($scope.page), 
			  headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
			 }).success(function($data) {
			 	messageBoxOk("Página salva com sucesso", "Salvar");
			 	if($data.inserted)
			 	{
			 		loadPages();
			 	}
    		}).error(function(){
    			messageBoxError("Erro ao salvar página", "Erro");
    		});
	};

	$scope.processFormUpdate = function()
	{
		var id = $("#page_id").val();
		var json_url = base_url+"/api/pages/save/" + id;
		$http({
			  method  : 'POST',
			  url     : json_url,
			  data    : $.param($scope.page), 
			  headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
			 }).success(function($data) {
			 	messageBoxOk("Página salva com sucesso", "Salvar");
			 	console.log($data);
    		}).error(function(){
    			messageBoxError("Erro ao salvar página", "Erro");
    		});
	};

	$scope.deletePage = function(id)
	{
		var json_url = base_url+"/api/pages/delete/" + id;
		$http.get(json_url).success(function($data){
			if($data.deleted){
				messageBoxOk("Página deletada com sucesso", "Deletar");
			}
		}).error(function(){
    		messageBoxError("Erro ao deletar página", "Erro");
    	});

    	loadPages();
	};

	$scope.uploadImage = function()
	{
		var id = $("#page_id").val();
		
		var uploadUrl = base_url+"/api/pages/image/save/" + id;
		var text = null;
		var file = $scope.myFile;
		$data = fileUpload.uploadFileToUrl(file, uploadUrl, text);
		console.log($data);
	};


	$scope.uploadFile = function()
	{
		$("#btnSaveFile").prop('disabled', true);
		var file = $scope.myFile;
		var uploadUrl = base_url+"api/file/upload";
		var text = null;
		$data = fileUpload.uploadFileToUrl(file, uploadUrl, text);
	};


	loadPage();
	loadPages();

});

angular.module("appAlumiplac").directive('fileModel', ['$parse', function ($parse) {
    return {
    restrict: 'A',
    link: function(scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;

        element.bind('change', function(){
            scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
            });
        });
    }
   };
}]);

angular.module("appAlumiplac").service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl, name){
         var fd = new FormData();
         fd.append('file', file);
         fd.append('name', name);
         $http.post(uploadUrl, fd, {
             transformRequest: angular.identity,
             headers: {'Content-Type': undefined,'Process-Data': false}
         })
         .success(function($data){
         	messageBoxOk("Upload de imagem concluído com sucesso", "Upload");
         	return $data;
         })
         .error(function(){
         	messageBoxError("Erro ao fazer upload da imagem", "Erro");
            return null;
         });
     }
 }]);