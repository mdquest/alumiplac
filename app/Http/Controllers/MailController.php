<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\Contact;
use \App\Newsletter;
use \App\Helpers;

class MailController extends Controller
{
    public function home_mail_contact(Request $request)
    {
    	$countContacts = collect(DB::SELECT("SELECT count(*) as total FROM contacts c"))->first()->total;
        $filter = $request->input('filter');

        if($filter == null || $filter == ""){
            $filter = "all";
        }

    	$user = Auth::user();
    	return view('mail.contact', ['countContacts' => $countContacts, 'user' => $user, 'filter' => $filter]);
    }

    public function mail_contact_print($id)
    {
        $user = Auth::user();
        $countContacts = collect(DB::SELECT("SELECT count(*) as total FROM contacts c"))->first()->total;
        
        $contact = collect(DB::SELECT("SELECT c.id, c.name, c.email, c.subject, c.content, c.created_at, e.id as estimate_id FROM contacts c LEFT JOIN estimates e ON e.contact_id=c.id WHERE c.id=?", [$id]))->first();

        $files = DB::SELECT("select f.id, f.location, f.file_type, f.created_at, f.created_by, f.name from estimates e join contacts c on c.id=e.contact_id join estimate_has_files ehf on ehf.estimate_id=e.id join files f on f.id=ehf.file_id where c.id=?", [$id]);   

        return view('mail.print', ['user' => $user, 'countContacts' => $countContacts, 'contact' => $contact, 'files' =>$files]);
    }

    public function home_mail_contact_read($id)
    {
        $user = Auth::user();
        $countContacts = collect(DB::SELECT("SELECT count(*) as total FROM contacts c"))->first()->total;
        
        $contact = collect(DB::SELECT("SELECT c.id, c.name, c.email, c.subject, c.content, c.created_at, e.id as estimate_id FROM contacts c LEFT JOIN estimates e ON e.contact_id=c.id WHERE c.id=?", [$id]))->first();

        $files = DB::SELECT("select f.id, f.location, f.file_type, f.created_at, f.created_by, f.name from estimates e join contacts c on c.id=e.contact_id join estimate_has_files ehf on ehf.estimate_id=e.id join files f on f.id=ehf.file_id where c.id=?", [$id]);   

        return view('mail.read', ['user' => $user, 'countContacts' => $countContacts, 'contact' => $contact, 'files' =>$files]);
    }

    public function api_mail_contact($filter, $page)
    {   
        $itemsPerPage = 12;

        $start = ($itemsPerPage * $page);
        $end =  $itemsPerPage + ($itemsPerPage * $page);

        $countItems = collect(DB::select("SELECT count(*) as total FROM contacts"))->first()->total;
        $countPages = ceil($countItems / $itemsPerPage);

        if($filter == "estimates")
        {
             $contacts = DB::SELECT("SELECT c.id,  c.email, c.name, c.subject as title, c.content, (SELECT count(*) FROM estimates e WHERE e.contact_id=c.id) as qt_estimate, c.created_at FROM contacts c JOIN estimates e ON e.contact_id=c.id ORDER BY c.created_at DESC LIMIT ?, ?", [$start, $itemsPerPage]);

        }else{
            $contacts = DB::SELECT("SELECT c.id,  c.email, c.name, c.subject as title, c.content, (SELECT count(*) FROM estimates e WHERE e.contact_id=c.id) as qt_estimate, c.created_at FROM contacts c ORDER BY c.created_at DESC LIMIT ?, ?", [$start, $itemsPerPage]);
        }

        $data =  array('pages' => $countPages, 'itemsPerPage' => $itemsPerPage,'items' => $countItems ,'currentPage' => $page, 'begin' => $start, 'end' => $end, 'contacts' => $contacts);
        return response()->json($data);
    }


    public function admin_mail_contact_create(Request $request)
    {
    	if($request->has('name') && $request->has('email') && $request->has('content') && $request->has('subject')){
	 		Contact::create($request->all());

	 		$company = \App\Company::where('active', 1)->first();

    		$request->request->add(['company_name' => $company->name]);

	 		$settings = \App\Setting::where('active', 1)->first();

	 		if($settings->send_email_on_contact)
	 		{
	 			$inserted = \App\Helpers\MailHelper::instance()->sendEmailContact($request->all());
	 		}

	        $data = array('inserted' => true);
    	}else{
    		$data = array('inserted' => false);
    	}
        return response()->json($data);
    }

    public function admin_email_create(Request $request)
    {
    	$user = Auth::user();
    	$short_user_name = \App\Helpers\StringHelper::instance()->get_first_name($user->name);

    	$company = \App\Company::where('active', 1)->first();

    	$request->request->add(['company_name' => $company->name]);
    	$request->request->add(['user_name' => $short_user_name]);

    	if($request->has('email') && $request->has('subject') && $request->has('content'))
    	{
    		$inserted = \App\Helpers\MailHelper::instance()->sendEmailCommon($request->all(), $user);
    		$data = array('sent' => true);
    	}else{
    			$data = array('sent' => false);    		
    	}

    	return response()->json($data);

    }

    public function admin_newsletter_create(Request $request)
    {
    	if($request->has('email'))
    	{
    		$email = $request->input('name');
    		$newsEmails = \App\Newsletter::where('email', $email);

    		if(!(count($newsEmails) > 0)){
    			Newsletter::create($request->all());
    		}

    		$data = array('inserted' => true);
    	}else{
    		$data = array('inserted' => false);
    	}

    	return response()->json($data);
    }
}
