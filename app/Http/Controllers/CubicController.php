<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\Estimate;
use \App\Contact;
use \App\User;
use \App\Company;
use Illuminate\Support\Facades\DB;

class CubicController extends Controller
{
    public function home_cubic_index()
    {
    	$user = Auth::user();
        $company = \App\Company::where('active', 1)->first();
    	return view('cubic.index', ['user'=>$user, 'company'=> $company]);
    }

    public function api_cubic_load($api_key)
    {

    	$company = \App\Company::where('active', 1)->first();

    	$sendMailTo = DB::SELECT("SELECT u.email FROM users u JOIN user_has_groups uhg ON uhg.user_id=u.id JOIN user_groups ug ON uhg.group_id=ug.id WHERE ug.code='ADMIN'");

            

    	$data = array('sendMailTo' => $sendMailTo, 'content'=> '', 'origin' => $company->name, 'origin_email'=> $company->default_email);


    	return response()->json($data);
    }
}
