<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Company;
use \App\Page;

class IndexController extends Controller
{
    public function index_index()
    {
    	//Company
        $company = \App\Company::where('active', 1)->first();
        $page = \App\Page::where('url', 'index')->first();
        $products = \App\Page::where('is_product', 1)->get();

        return view('welcome', ['company' => $company, 'page' => $page, 'products' => $products, 'isIndex' => true]);
    }
}
