<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use \App\Page;
use \App\Gallery;

class PagesController extends Controller
{
	public function __construct()
    {
        //$this->middleware('auth');
    }

    public function home_pages_index()
    {
    	$user = Auth::user();
    	
    	return view('pages.index', ['user' => $user]);
    }

    public function api_get_pages()
    {
        $pages = DB::SELECT("SELECT p.id, p.name, p.created_at, p.url, p.is_product, u.name as user_name FROM pages p JOIN users u on u.id=p.created_by");
        return response()->json($pages);
    }

    public function home_create_page()
    {

    }

    public function api_pages_save(Request $request)
    {
        $id = $request->input('id');
        $page = \App\Page::where('id', $id)->first();

        $page->description = $request->input('description');
        $page->name = $request->input('name');
        $page->is_product = $request->input('is_product')?1:0;
        $page->is_blog = $request->input('is_blog')?1:0;
        $page->content = $request->input('content');
        $page->keywords = $request->input('keywords');
        $page->url = $request->input('url');

        //$page->fill($request->all());
        $page->save();

        return response()->json(array('inserted' => true));
    }

    public function api_pages_delete($id)
    {
        $page = \App\Page::find($id);
        $deleted = false;

        if($page != null){
            $deleted = $page->delete();
        }

        return response()->json(array('deleted' => $deleted));
    }

    public function api_pages_create(Request $request)
    {
        $page = new Page;
        $page->name = $request->input('name');
        $page->is_product = $request->input('is_product')?1:0;
        $page->url = $request->input('url');

        //$page->fill($request->all());
        $page->save();

        if($page->save()) {
            $gallery = new Gallery;
            $gallery->page_id = $page->id;
            $gallery->save();


            return response()->json(array('inserted' => true, 'id' => $page->id));
        }else{
            return response()->json(array('inserted' => false, 'id' => $page->id));
        }
    }

    public function api_update_page_image( Request $request,$id)
    {
        $page = \App\Page::where('id', $id)->first();
        $file =  $request->file('file');
        $extension = $file->getClientOriginalExtension();

        $new_file_name = md5(uniqid(rand(), true)).'.'.$extension;
        Storage::disk('local')->put($new_file_name,  File::get($file));

        $page->image = 'storage/app/'.$new_file_name;
        $page->save();

        $data = array('deleted' => true, 'file_url' => 'storage/app/'.$new_file_name);
        return response()->json($data);
    }

    public function save_on_gallery($file_id, $page_id)
    {
        $gallery = collect(DB::SELECT("select gallery_has_file_id from gallery where page_id=?", [$page_id]))->first();

        DB::table('gallery_has_file')->insert(
            ['file_id' => $file_id, 'gallery_id' => $gallery->gallery_has_file_id]
        );

        $data = array('inserted' => true );
        return response()->json($data);
    }

    public function api_gallery_page($id)
    {
        $gallery = DB::SELECT("SELECT * FROM gallery g JOIN gallery_has_file ghf ON ghf.gallery_id=g.gallery_has_file_id JOIN files f ON f.id=ghf.file_id WHERE g.page_id=? ORDER BY file_id DESC", [$id]);
        return response()->json($gallery);
    }

    public function api_edit_page($id)
    {
         $page = collect(DB::SELECT("SELECT * FROM pages p WHERE p.id=?", [$id]))->first(); 
         return response()->json($page);
    }

    public function api_gallery_delete($id)
    {
        $deleted = DB::table('gallery_has_file')->where('file_id', $id)->delete();
        $data = array('deleted' => true);
        return response()->json($data);
    }

    public function home_edit_page($id)
    {
        $user = Auth::user();
        return view('pages.edit', ['user' => $user, 'page_id' => $id ]);
    }

    public function home_gallery_page($id)
    {
        $user = Auth::user();
        return view('pages.gallery', ['user' => $user, 'page_id' => $id ]);
    }

    public function home_pages_handler($page)
    {
    	$db_page = collect(DB::SELECT("SELECT * FROM pages p WHERE p.url=?", [$page]))->first();
        $company = \App\Company::where('active', 1)->first();
        $products = \App\Page::where('is_product', 1)->get();

        $gallery = DB::SELECT("SELECT * FROM gallery g JOIN gallery_has_file ghf ON ghf.gallery_id=g.gallery_has_file_id JOIN files f ON f.id=ghf.file_id WHERE g.page_id=?", [$db_page->id]);

    	return view('pages.handler', ['db_page' => $db_page, 'company' => $company, 'products' => $products, 'gallery'=>$gallery]);
    }
}
