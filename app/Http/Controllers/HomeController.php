<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Helpers;
use Illuminate\Support\Facades\DB;

use \App\Estimate;
use \App\Contact;
use \App\User;
use \App\Company;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function about_this_project()
    {
        $user = Auth::user();
        return view('project', ['user' => $user]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        //Orçamentos
        $count_estimates = count(\App\Estimate::all());
        $count_newletter = count(\App\Newsletter::all());
        //Contatos
        $contacts = \App\Contact::all();
        //Users
        $users = \App\User::all();

        $estimates = DB::select("SELECT c.name, c.email, c.subject, c.content, mt.id, mt.name as material, (SELECT count(*) FROM contact_answers ca WHERE ca.contact_id=c.id) as answered_times FROM estimates e JOIN contacts c ON e.contact_id=c.id JOIN material_types mt ON mt.id=e.material_type_id ORDER BY c.id DESC LIMIT 20");

        $newst_users = DB::select("SELECT * FROM users u ORDER BY u.id ASC LIMIT 8");
        

        return view('home', ['user' => $user, 'estimates'=> $estimates, 'users' => $users, 'count_estimates' => $count_estimates, 'contacts' => $contacts, 'newst_users' =>  $newst_users,'count_newletter' => $count_newletter]);
    }
}
