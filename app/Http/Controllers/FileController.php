<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class FileController extends Controller
{
    public function home_get_file($file)
    {
    	return response()->file('storage/app/'.$name);
    }

    public function save_file(Request $request)
    {
        $file =  $request->file('file');
        $extension = $file->getClientOriginalExtension();

        $new_file_name = md5(uniqid(rand(), true)).'.'.$extension;
        Storage::disk('local')->put($new_file_name,  File::get($file));

        $url = 'storage/app/'.$new_file_name;
        

        $id = DB::table('files')->insertGetId(
		    ['location' => $new_file_name, 'file_type' => $extension, 'name' => $file]
		);

        return response()->json(array('inserted' => true, 'id' => $id,'file_url' => 'storage/app/'.$new_file_name, 'extension' => $extension));
    }
}
