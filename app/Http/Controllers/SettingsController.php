<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class SettingsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function home_settings_company()
    {
    	$user = Auth::user();    	
    	return view('settings.company', ['user' => $user]);
    }

    public function admin_settings_company()
    {
        $company = \App\Company::where('active', 1)->first();
        return response()->json($company);
    }

    public function home_settings_analytics()
    {
        $user = Auth::user();       
        return view('settings.analytics', ['user' => $user]);
    }

    public function save_settings_company(Request $request)
    {
        $company = \App\Company::where('active', 1)->first();
        $company->fill($request->all());
        $company->save();
        return response()->json(array('inserted' => true));
    }

    public function save_settings_company_logo(Request $request)
    {
        $company = \App\Company::where('active', 1)->first();
        
        $file =  $request->file('file');
        $extension = $file->getClientOriginalExtension();

        $new_file_name = md5(uniqid(rand(), true)).'.'.$extension;
        Storage::disk('local')->put($new_file_name,  File::get($file));

        $company->image = 'storage/app/'.$new_file_name;
        $company->save();

        return response()->json(array('inserted' => true, 'image' => $new_file_name, 'file_url' => 'storage/app/'.$new_file_name));
    }
}
