<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EstimateController extends Controller
{
    public function app_estimate_index()
    {
    	//Company
        $company = \App\Company::where('active', 1)->first();
        $page = \App\Page::where('url', 'orcamento')->first();
        $products = \App\Page::where('is_product', 1)->get();

        $materials = DB::SELECT("SELECT * FROM material_types");
        $fix_types = DB::SELECT("SELECT * FROM fix_type");

        return view('estimate.index', ['company' => $company, 'page' => $page, 'products' => $products, 'materials' => $materials, 'fix_types' => $fix_types]);

    }

    public function save_estimate_file(Request $request){
    	$file =  $request->file('file');
    	$extension = $file->getClientOriginalExtension();

        $new_file_name = md5(uniqid(rand(), true)).'.'.$extension;
        Storage::disk('local')->put($new_file_name,  File::get($file));


        $id = DB::table('files')->insert(
			    ['location' => $new_file_name, 'file_type' => $extension, 'name' => $file]
			);

        return response()->json(array('inserted' => true, 'image' => $new_file_name, 'file_url' => 'storage/app/'.$new_file_name, 'id' => $id));
    }
}
