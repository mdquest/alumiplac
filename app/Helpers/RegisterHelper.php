<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Mail;

class RegisterHelper
{
	public static function instance()
    {
        return new RegisterHelper();
    }

    public function is_allow_user_register()
    {
    	return collect(DB::SELECT("SELECT allow_user_register as allow FROM settings"))->first()->allow == 1;
    }
}

