<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Mail;

class MailHelper
{

	public function sendEmailContact($data)
	{
		Mail::send('emails.contact', $data, function($message) use ($data)
            {
            	$company = \App\Company::where('active', 1)->first();

            	$appEmail = config('app.mail', 'alumiplac.etiquetas@gmail.com');

                $message->from($appEmail, $company->name);
                $message->subject("Nova mensagem na plataforma");
                $message->to($company->default_email);
            });
	}

	public function sendEmailCommon($data, $user)
	{
		Mail::send('emails.sendmail', $data, function($message) use ($data, $user)
            {
            	$company = \App\Company::where('active', 1)->first();

            	$appEmail = config('app.mail', 'alumiplac.etiquetas@gmail.com');

                $message->from($appEmail, $company->name);
                $message->subject($data['subject']);
                $message->to($data['email']);
            });
	}

	public static function instance()
    {
        return new MailHelper();
    }

}