<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class StringHelper
{

	public function get_first_name($fullName)
	{
		$nameParts = explode(' ', $fullName);
	 return $nameParts[0];
	}

	public static function instance()
    {
        return new StringHelper();
    }

    public function file_name_to_icon($file_name)
    {
    	 $array = explode(".", $file_name);
         $file_type = end($array);

         $icon = 'fa fa-file';

         switch ($file_type) {
          	case 'pdf':
          		$icon = 'fa fa-file-pdf-o';
          		break;
          	case 'docx':
          		$icon = 'fa fa-file-word-o';
          		break;
          	case 'doc':
          		$icon = 'fa fa-file-word-o';
          		break;
          	case 'jpg':
          		$icon = 'fa fa-image';
          		break;
          	case 'png':
          		$icon = 'fa fa-image';
          		break;
          } 

          return $icon;
    }

}