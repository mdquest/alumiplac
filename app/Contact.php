<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
   protected $table = 'contacts';

   protected $fillable = ['id', 'email', 'name', 'title', 'message', 'business_name', 'phone_number', 'created_at', 'updated_at'];
}
