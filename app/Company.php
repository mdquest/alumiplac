<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
     protected $fillable = ['id', 'name', 'description', 'keywords', 'position', 'place', 'region', 'mission', 'vision', 'values', 'about', 'active', 'default_email'];
}
