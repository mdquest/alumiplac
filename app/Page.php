<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    protected $fillable = ['id', 'url', 'name', 'content', 'created_at', 'updated_at', 'keywords', 'description', 'is_product', 'image', 'is_blog', 'is_active', 'created_by' ];
}
