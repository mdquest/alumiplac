<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'pages';
    protected $fillable = ['page_id', 'gallery_has_file_id'];
}
